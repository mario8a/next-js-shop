import React, { useRef } from 'react';

import logotipo from '../assets/logos/logo_yard_sale.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from '@styles/Login.module.scss';

const Login = () => {
   const form = useRef(null);

   const handleSubmit = (event) => {
      event.preventDefault();
      const formData = new FormData(form.current);
      const data = {
         username: formData.get('email'),
         password: formData.get('password')
      };
      console.log(data);
   };

  return (
     <div className={styles.Login}>
        <div className={styles['Login-container']}>
            <Image src={logotipo} alt="" className={styles.logoEmpresa}/>
           <form action="/" className={styles.form} ref={form}>
              <label htmlFor="email" className={styles.label}>
                 Email address
              </label>
              <input
                 type="text"
                 name="email"
                 placeholder="platzi@example.cm"
                 className={styles['input', 'input-email']}
              />
              <label htmlFor="password" className="{styles.label}">
                 Password
              </label>
              <input
                 type="password"
                 name="password"
                 placeholder="*********"
                 className={styles['input','input-password']}
              />
              <button
                 className={styles['primary-button', 'login-button']}
                 onClick={handleSubmit}
              >Login</button>
              <Link href="/">Forgot my password</Link>
           </form>
            <Link href="/signup" className={styles['secondary-button', 'signup-button']}>
               Sign up
            </Link>
        </div>
     </div>
  );
};

export default Login;