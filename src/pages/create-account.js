import React, { useRef } from 'react';

import styles from '@styles/CreateAccount.module.scss';

const CreateAccount = () => {

	const form = useRef(null);

   const handleSubmit = (event) => {
      event.preventDefault();
      const formData = new FormData(form.current);
      const data = {
         username: formData.get('name'),
				 email: formData.get('email'),
         password: formData.get('password')
      };
      console.log(data);
   };

  return (
     <div className={styles.CreateAccount}>
        <div className={styles['CreateAccount-container']}>
           <h1 className={styles.title}>My account</h1>
           <form action="/" className={styles.form} ref={form}>
              <div>
                 <label htmlFor="name" className={styles.label}>
                    Name
                 </label>
                 <input
                    type="text"
                    name="name"
                    placeholder="Teff"
                    className={styles.input, styles['input-name']}
                 />
                 <label htmlFor="email" className={styles.label}>
                    Email
                 </label>
                 <input
                    type="text"
                    name="email"
                    placeholder="platzi@example.com"
                    className={styles.input, styles['input-email']}
                 />
                 <label htmlFor="password" className={styles.label}>
                    Password
                 </label>
                 <input
                    type="password"
                    name="password"
                    placeholder="*********"
                    className={styles.input, styles['input-password']}
                 />
              </div>
              <button
                 className={styles['primary-button'], styles['login-button']}
								 onClick={handleSubmit}>
								Create
							</button>
           </form>
        </div>
     </div>
  );
};

export default CreateAccount;
