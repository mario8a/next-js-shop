import React from 'react';
import '@styles/Order.scss';
import Image from 'next/image';
import flechita from '@icons/flechita.svg';

const Order = () => {
  return (
    <div className="Order">
			<p>
				<span>03.25.21</span>
				<span>6 articles</span>
			</p>
			<p>$560.00</p>
			<Image src={flechita.src} alt="arrow" width={140} height={150} />
		</div>
  );
};

export default Order;
