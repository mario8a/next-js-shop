/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useContext, useEffect } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import Menu from '@components/Menu';
import menu from '@icons/icon_menu.svg';
import logo from '@logos/logo_yard_sale.svg';
import shoppingCart from '@icons/icon_shopping_cart.svg';
import AppContext from '@context/AppContext';
import MyOrder from '@containers/MyOrder';
import styles from '@styles/Header.module.scss';

const Header = () => {
	const [toggle, setToggle] = useState(false);
	const [toggleOrders, setToggleOrders] = useState(false);
	// const [offset, setOffset] = useState(0);
	const { state } = useContext(AppContext);

	const handleToogle = () => {
		setToggle(!toggle);
	};

	useEffect(() => {
    window.onscroll = () => {
			if (window.pageYOffset > 500) {
				setToggle(false);
			}
      if (window.pageYOffset > 800) {
				setToggleOrders(false);
			}
    };
  }, []);
	
	return (
		<nav className={styles.Nav}>
			<img src={menu.src} alt="menu" className={styles.menu} />
			<div className={styles['navbar-left']}>
				<Link href="/" passHref>
					<Image src={logo} alt="logo" className={styles['nav-logo']} />
				</Link>	
				<ul>
					<li>
						<Link href="/">All</Link>
					</li>
					<li>
						<Link href="/">Clothes</Link>
					</li>
					<li>
						<Link href="/">Electronics</Link>
					</li>
					<li>
						<Link href="/">Furnitures</Link>
					</li>
					<li>
						<Link href="/">Toys</Link>
					</li>
					<li>
						<Link href="/">Others</Link>
					</li>
				</ul>
			</div>
			<div className={styles['navbar-right']}>
				<ul>
					<li className={styles["navbar-email"]} onClick={handleToogle}>
						platzi@example.com
					</li>
					<li className={styles['navbar-shopping-cart']} onClick={() => setToggleOrders(!toggleOrders)}>
						<Image src={shoppingCart} alt="shopping cart" width={30} height={30}/>
						{state.cart.length > 0 ? <div>{state.cart.length}</div> : null} 
					</li>
				</ul>
			</div>
			{ toggle &&  <Menu /> }
			{ toggleOrders && <MyOrder toogleOrder={setToggleOrders} /> }
		</nav>
	);
};

export default Header;