import React, { useContext } from 'react';
import Image from 'next/image';
import closeIcon from '../assets/icons/icon_close.png';
import AppContext from '@context/AppContext';

import styles from '@styles/OrderItem.module.scss';

const OrderItem = ({product}) => {

	const { removeToCart } = useContext(AppContext);
	
	const handleRemove = product => {
		removeToCart(product);
	};

  return (
    <div className={styles.OrderItem}>
			<figure>
				<Image src={product?.images[0]} alt={product?.title} width={140} height={150}/>
			</figure>
			<p>{product?.title}</p>
			<p>${product?.price}</p>
			<Image src={closeIcon} alt="close"  onClick={() => handleRemove(product)} />
		</div>
  );
};

export default OrderItem;
