/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, {useContext} from 'react';
import Image from 'next/image';
import AppContext from '@context/AppContext';

import addCarImage from '@icons/bt_add_to_cart.svg';
import styles from '@styles/ProductItem.module.scss';

const ProductItem = ({product}) => {
	// const [cart, setCart] = useState();
	const { addToCart } = useContext(AppContext);

	//Antes () -> Despues -> item
	const handleCart = item => {
		addToCart(item);
	};
	
	
  return (
    <div className={styles.ProductItem}>
			<Image src={product.images[0]} width={240} height={240} alt={product.title} />
			<div className={styles['product-info']}>
				<div>
					<p>${product.price}</p>
					<p>{product.title}</p>
				</div>
				<figure onClick={() => handleCart(product)}>
					<Image width={40} height={40} src={addCarImage} alt="" />
				</figure>
			</div>
		</div>
  );
};

export default ProductItem;
