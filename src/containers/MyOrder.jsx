import React, { useContext } from 'react';
import Link from 'next/link';
import OrderItem from '@components/OrderItem';

import flechita from '@icons/flechita.svg';
import AppContext from '@context/AppContext';

import styles from '@styles/MyOrder.module.scss';
import Image from 'next/image';

const MyOrder = ({toogleOrder}) => {
	const { state } = useContext(AppContext);

	const sumTotal = () => {
		const reducer = (acumulador, currentValue) => acumulador + currentValue.price;
		const sum = state.cart.reduce(reducer, 0);
		return sum;
	};

  return (
    <aside className={styles.MyOrder}>
			<div className="title-container">
				<Image src={flechita} alt="arrow" onClick={() => toogleOrder(false)}/>
				<p className="title">My order</p>
			</div>
			<div className="my-order-content">
				{ state.cart.map(product => (
					<OrderItem product={product} key={`orderItem-${product.id}`}/>
				))}
				<div className="order">
					<p>
						<span>Total</span>
					</p>
					<p>${sumTotal()}</p>
				</div>
					<Link className="primary-button" onClick={() => toogleOrder(false)} href="/checkout">
						Checkout
					</Link>
			</div>
		</aside>
  );
};

export default MyOrder;
